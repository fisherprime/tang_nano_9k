# tang_nano_9k

This repository contains projects synthesizable into bitstreams for the Tang Nano 9K FPGA.

## Usage

### Open source toolchain

This flow uses `yosys`, `nextpnr`(gowin specific) & `prjapicula` to generate the bitstream.

**Uploading the generated bitstream fails with: Failed to claim FPGA device: mismatch between
target's idcode and fs idcode.**

```
DEVICE=GW1NR-LV9QN88PC6/I5
yosys -p "read_verilog -sv ${FILE.sv}; synth_gowin -json ${SYNTH.json}"
nextpnr-gowin --device $DEVICE --cst ${CONSTRAINT.cst} --json ${SYNTH.json} --write ${PNR.json}
gowin_pack -d $DEVICE -o ${BITSTR.fs} ${PNR.json}
openFPGALoader -b tangnano9k -f ${BITSTR.fs}
```

### Proprietary toolchain

The gowin IDE's synthesis and place-and-route steps work on Linux-based systems.
`openFPGALoader` is required to upload the generated bitstream onto the FPGA as the IDE's programmer
path is hardcoded & is mismatched on Linux-based systems.
